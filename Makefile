.PHONY: build check push
# Container registry that'll prefix the image name
CR := auroralabs
NAME := exam
# At the very least, we want to tag the image
# in way that's easily related to the repo (commit sha).
# Depending on joint decisions about the dev flow, we'd
# also tag with a version out of a file
# and/or git tag and/or incremented build number.
COMMIT_SHA := $(shell git rev-parse --short HEAD)
VERSION := $(shell cat app/__version__.py)
APP_TAG := $(CR)/$(NAME):$(COMMIT_SHA)
VERSION_TAG = $(CR)/$(NAME):$(VERSION)
TEST_TAG := $(CR)/$(NAME):$(COMMIT_SHA)-test

build:
	docker build --target app --tag $(APP_TAG) .

check:
	docker build --target test --tag $(TEST_TAG) .
	mkdir -p $(PWD)/report
	# If the test fails, the exit code
	# propagates and the CI will fail.
	# The test creates a junit report consumed by the CI.
	docker run -v $(PWD)/report:/tmp/report --rm $(TEST_TAG)

push:
	docker build --target app --tag $(APP_TAG) .
	docker tag $(APP_TAG) $(VERSION_TAG)
	docker push $(APP_TAG)
	docker push $(VERSION_TAG)
