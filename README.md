# AuroraLabs Devops Exam
A rich CLI tool that says something great.

## CI
* Builds the container image
  * Multistage Dockerfile to target build but also utilize cached layers between `app` and `test`.
  * Stage `test` creates a layer with `app`, saving execution time and disk.
  * In a real world scenario, we'd use a rootless builder such as [`kaniko`](https://github.com/GoogleContainerTools/kaniko/blob/main/docs/tutorial.md)
* Tests the app
  * Uses `pytest` to produce a `junit` report.
  * `junit` reports integrate with a myriad of tools, including Jenkins plugins.
* Pushes the container image to a Container Registry.
  * Authentication can be managed using `docker login`.
  * Credentials depend on the CI and CR combination.
  * Straight forward Service Account + Role with r/w to the CR Storage in [GCR](https://cloud.google.com/container-registry/docs/advanced-authentication#standalone-helper)
  * Using [GitLab CI context aware](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html#use-gitlab-cicd-to-authenticate) (that dynamically creates a one-time job key with authorization to the GitLab CR)

## Notes
### Artifacts
In case we want to produce and publish
different artifacts, in the domain of Python+Microservices.
* PyPI packages
  * Indices such as Jfrog, Nexus, GitLab
  * Poetry simplifies the process with `poetry publish` (after adding a 3rd party index and authenticating)
  * GitLab CI simplifies the process by creating CI job tokens with configured permissions (r/w to corporate GitLab PyPI index)
* Helm Charts
  * Jfron, Nexus and GitLab support Chart Repositories
  * Chart Repository protocol officially supports pushing charts using `curl`

### Security
Depending on the CI used (GitLab/Jenkins/etc), a
 few more features/plugins need to be enabled/added differently.
* Static code analysis.
* Vulnerable dependency management (such as `dependabot`)
  * major/minor/path to be upgraded in the merge request?
  * What's the playbook for different groups of dependancies, such as internal packages and 3rd party ones (how to handle/who to contact)
* Container image scanner
  * Snyk
  * Harbor
  * GCR
