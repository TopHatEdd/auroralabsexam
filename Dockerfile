# "Our" builder.
# Either we use a reputable and suitable builder 
# that's maintained upstream such as 
# gcr.io/cloud-builders
# Or manage our own using trusted sources (Ubuntu/AlmaLinux package manager, etc)
FROM fnndsc/python-poetry:1.3.1 AS app

    # prevents python creating .pyc files
ENV PYTHONDONTWRITEBYTECODE=1 \
    \
    # disable pip cache and upgrade check
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    \
    # poetry do not ask any interactive question
    POETRY_NO_INTERACTION=1 \
    WORKDIR=/app

WORKDIR $WORKDIR
# Make a cache layer with our dependencies
# which don't frequently change.
COPY poetry.lock pyproject.toml ./
# Don't install dev dependencies (such as pytest, black, etc)
RUN poetry install --without=dev
# Only after installing dependencies,
# make a layer with our app that changes frequently.
COPY ./app/ ./

# Ideally, we'd want ENTRYPOINT as something that doesn't change often,
# such as executing our module/package.
# and leaving CMD empty for the user to supply CLI flags.
ENTRYPOINT ["python"]
CMD ["main.py"]

FROM app AS test

WORKDIR $WORKDIR

# This time, install dev dependencies because we
# need pytest
RUN poetry install --with=dev

# Run our tests and generate the test report
# Options are managed in our toml config.
ENTRYPOINT ["pytest"]
